import { BasicAuthInterceptor } from './_helpers/basic-auth.interceptor';
import { SalesDataService } from './salesdata.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; // add this
import { FormsModule, ReactiveFormsModule } from '@angular/forms';// add this


import { AppComponent } from './app.component';
import { UserService } from './user.service'; // add this
import { AppRoutingModule } from './app-routing.module';

import {MatStepperModule} from '@angular/material/stepper';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { PricePredictionComponent } from './price-prediction/price-prediction.component';
import { CompetitivePricePredictionComponent } from './competitive-price-prediction/competitive-price-prediction.component';
import { PricesSalesVisualizationComponent } from './prices-sales-visualization/prices-sales-visualization.component';
import { DemandPredictionComponent } from './demand-prediction/demand-prediction.component';

import { CsvUploadComponent } from './components/csv-upload/csv-upload.component';
import { CsvReadComponent } from './components/csv-read-dialog/csv-read.component';
import { LogoutDialogComponent } from './components/logout-dialog/logout-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    TopMenuComponent,
    PricesSalesVisualizationComponent,
    LogoutDialogComponent,
    PricePredictionComponent,
    DemandPredictionComponent,
    CompetitivePricePredictionComponent,
    CsvUploadComponent,
    CsvReadComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,

    MatSliderModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    MatInputModule,
    MatTabsModule,
    MatGridListModule,
    MatDividerModule,
    MatDialogModule,
    MatSelectModule,
    MatStepperModule,

    AppRoutingModule,
    CommonModule,
    BrowserAnimationsModule,

    NgxChartsModule,
  ], // add this
  providers: [
    UserService,
    SalesDataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptor,
      multi: true,
    },
    CompetitivePricePredictionComponent
  ], // add this

  bootstrap: [AppComponent],
})
export class AppModule {}
