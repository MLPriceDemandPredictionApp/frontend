import { LoginComponent } from './login/login.component';
import { Component } from '@angular/core';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PricesSalesVisualizationComponent } from './prices-sales-visualization/prices-sales-visualization.component';
import { AuthGuard, LoginGuard } from './_helpers/auth.guard';
import { PricePredictionComponent } from './price-prediction/price-prediction.component';
import { DemandPredictionComponent } from './demand-prediction/demand-prediction.component';
import { CompetitivePricePredictionComponent } from './competitive-price-prediction/competitive-price-prediction.component';
// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }

import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
// import {UserComponent} from '../user/user.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  {
    path: 'pricesSales',
    component: PricesSalesVisualizationComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pricePrediction',
    component: PricePredictionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'demandPrediction',
    component: DemandPredictionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'competitivePricePrediction',
    component: CompetitivePricePredictionComponent,
    canActivate: [AuthGuard],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
