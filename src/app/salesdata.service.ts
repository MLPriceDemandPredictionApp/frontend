import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from './user.service';
import { BACKEND_URL } from './_helpers/constants'
 
@Injectable()
export class SalesDataService {

  private baseUrl: any = BACKEND_URL
 
  constructor(private http: HttpClient, private _userService: UserService) { }
 
  // Uses http.get() to load data from a single API endpoint
  list() {
    return this.http.get(`${this.baseUrl}/salesData/`);
  }
 
  // send a POST request to the API to create a new data object
  // create(post: any, token: any) {
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Authorization': 'JWT ' + this._userService.token   // this is our token from the UserService (see Part 1)
  //     })
  //   };
  //   return this.http.post('/api/posts', JSON.stringify(post), httpOptions);
  // }

  postCsvData(post: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token   // this is our token from the UserService (see Part 1)
      })
    };
    return this.http.post(`${this.baseUrl}/salesData/uploadCsv/`, JSON.stringify(post), httpOptions);
  }

  predict(post: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token   // this is our token from the UserService (see Part 1)
      })
    };
    return this.http.post(`${this.baseUrl}/salesData/predict/`, JSON.stringify(post), httpOptions);
  }

}