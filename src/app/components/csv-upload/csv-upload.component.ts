import { PricesSalesVisualizationComponent } from './../../prices-sales-visualization/prices-sales-visualization.component';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { CompetitivePricePredictionComponent } from 'src/app/competitive-price-prediction/competitive-price-prediction.component';

@Component({
  selector: 'csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.scss'],
})
export class CsvUploadComponent implements OnInit {
  firstFormGroup: any;
  secondFormGroup: any;
  stage: string = 'upload';
  csvData: any = [
    {
      Item: 'Salmon',
      Category: 'Seafood',
      Year: '2020',
      Month: '2',
      Date: '0',
      Price: '5.50',
    },
  ];
  sliderValue = 0;

  displayedColumns: string[] = [
    'Item',
    'Category',
    'Year',
    'Month',
    'Date',
    'Price',
    'Demand',
  ];

  dataSource = this.csvData;
  dataLength = this.csvData.length;

  constructor(public _userService: UserService, private router: Router, public PriceDemandViz: PricesSalesVisualizationComponent) {}

  ngOnInit(): void {
    // this.firstFormGroup = this._formBuilder.group({
    //   firstCtrl: ['', Validators.required]
    // });
    // this.secondFormGroup = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required]
    // });
  }

  logout() {
    this._userService.logout();
    // this.router.navigate(['/login']);
  }

  setStage(stage: string) {
    this.stage = stage;
  }

  csvToArray(csvString: any) {
    var lines = csvString.split('\n');
    var headerValues = lines[0].split(',');
    var dataValues = lines.splice(1).map(function (dataLine: any) {
      return dataLine.split(',');
    });
    return dataValues.map(function (rowValues: any) {
      var row: any = {};
      headerValues.forEach(function (headerValue: any, index: any) {
        row[headerValue] = index < rowValues.length ? rowValues[index] : null;
      });
      return row;
    });
  }

  public changeListener(event: any) {
    if ((event.target as any).files && (event.target as any).files.length) {
      const files = event.target.files;
      console.log(files);
      if (files && files.length > 0) {
        let file: any = files.item(0);
        console.log(file.name);
        console.log(file.size);
        console.log(file.type);
        let reader: FileReader = new FileReader();
        reader.readAsText(file);
        reader.onload = (e) => {
          let csv: string = reader.result as string;
          this.csvData = this.csvToArray(reader.result);
          console.log(this.csvData, reader.result);
        };
        this.dataSource = this.csvData;
        this.dataLength = this.csvData.length;
      }
    }
  }

  uploadCsvData() {
    console.log('upload', this.csvData)
    this.PriceDemandViz.uploadCsvData(this.csvData)
  }

  
}


