import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout-dialog',
  templateUrl: './logout-dialog.component.html',
  styleUrls: ['./logout-dialog.component.scss']
})
export class LogoutDialogComponent implements OnInit {

  constructor(public _userService: UserService, private router: Router,) { }

  ngOnInit(): void {
  }


  logout() {
    this._userService.logout();
    // this.router.navigate(['/login']);
  }

}
