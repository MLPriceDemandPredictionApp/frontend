import { LoginComponent } from './../../login/login.component';
import { UserService } from './../../user.service';
import { Component, OnInit } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { LogoutDialogComponent } from '../logout-dialog/logout-dialog.component';
@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss'],
})
export class TopMenuComponent implements OnInit {
  links = ['First', 'Second', 'Third'];
  activeLink = this.links[0];

  animal: any;
  name: any;

  constructor(
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  navigateHome() {
    this.router.navigate(['/']);
  }

  onTabClick(link: any) {
    this.activeLink = link;
  }

  isLoginPage() {
    // console.log(this.router.url);
    return this.router.url !== '/';
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LogoutDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }
}