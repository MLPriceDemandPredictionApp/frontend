import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {throwError} from 'rxjs';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
 
  /**
   * An object representing the user for the login form
   */
  public user: any;
  public isLoggedIn: any;
  
  constructor(public _userService: UserService, private router: Router) { 
    
  }
 
  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
    this.isLoggedIn = !this.isLoggedIn
  }
 
  login() {
    this._userService.login({'username': this.user.username, 'password': this.user.password});
  }
 
  refreshToken() {
    this._userService.refreshToken();
  }
 
  logout() {
    this._userService.logout();
  }
 
}