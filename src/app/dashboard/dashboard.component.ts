import { Component, OnInit } from '@angular/core';
import { TopMenuComponent } from '../components/top-menu/top-menu.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  links = [
    {
      title: 'Prices and Sales Visualization',
      description:
        "Use the query() method to find and animate elements within the current host component. statement returns the view that is being inserted. returns the view that is being removed. Let's assume that we are routing from the Home => About.",
      redirectTo: 'pricesSales',
    },
    { title: 'Price Prediction', description: 'fdlkadfsgf', redirectTo: 'pricePrediction'},
    { title: 'Demand Prediction', description: 'fdlkadfdkjsa;fdsffsgf', redirectTo: 'demandPrediction' },
    { title: 'Competitive Price Prediction', description: 'fdlkadfsgf', redirectTo: 'competitivePricePrediction' },
  ];
  constructor(private router: Router) {}

  ngOnInit(): void {
    console.log('dashboard init');
  }

  onLinkClick(link: any) {
    this.router.navigate([`/${link}`]);
  }
}
