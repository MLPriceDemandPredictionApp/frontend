import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesSalesVisualizationComponent } from './prices-sales-visualization.component';

describe('PricesSalesVisualizationComponent', () => {
  let component: PricesSalesVisualizationComponent;
  let fixture: ComponentFixture<PricesSalesVisualizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PricesSalesVisualizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesSalesVisualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
