import { Component, OnInit } from '@angular/core';
import { SalesDataService } from '../salesdata.service';
import { UserService } from '../user.service';
import { uniq, groupBy, sumBy } from 'lodash';
import { getDateString } from '../_helpers/utils';
import { CsvUploadComponent } from '../components/csv-upload/csv-upload.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

const single = [
  {
    name: 'Germany',
    value: 8940000,
  },
  {
    name: 'USA',
    value: 5000000,
  },
  {
    name: 'France',
    value: 7200000,
  },
];
@Component({
  selector: 'app-prices-sales-visualization',
  templateUrl: './prices-sales-visualization.component.html',
  styleUrls: ['./prices-sales-visualization.component.scss'],
})
export class PricesSalesVisualizationComponent implements OnInit {
  public data: any;
  public formattedData: any;

  view: [number, number] = [800, 600];

  public categories: any;
  types = ['Price', 'Demand'];
  periods = ['yearly', 'monthly', 'weekly'];

  //settings
  public type: any = 'Price';
  public category: any;
  public period: any;

  public multi: any;

  public disablePeriod: any;

  legendPosition: any = 'below';
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Time line';
  yAxisLabel: string = this.type;
  timeline: boolean = true;

  //bar graph props
  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  // showLegend = true;
  // showXAxisLabel = true;
  // xAxisLabel = 'Product';
  // showYAxisLabel = true;
  // yAxisLabel = 'Total';
  // single = single;
  // barPadding = 20;
  // showDataLabel = true;
  // roundEdges = true;

  // colorScheme = {
  //   domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  // };
  constructor(
    private _salesDataService: SalesDataService,
    private _userService: UserService,
    public dialog: MatDialog
  ) {
    Object.assign(this, { multi: this.formattedData });
  }

  ngOnInit(): void {
    console.log('PriceSalesVis init');
    this.getData();
  }
  
  convertGraphData(data: any[]): void {
    const filteredData = this.category
      ? this.data.filter((d: any) => d.category === this.category)
      : this.data;
    const groupedData = groupBy(filteredData, 'item');
    const formattedData = Object.keys(groupedData).map((item: any) => ({
      name: item,
      series: groupedData[item].map((date: any) => ({
        name: getDateString(date.date_time),
        value: this.type === 'Demand' ? date.demand : date.price,
      })),
    }));
    Object.assign(this, { multi: formattedData });
  }

  onSelectChange(event: any): void {
    this.convertGraphData(this.data);
    this.yAxisLabel = this.type;
  }

  getData() {
    this._salesDataService.list().subscribe(
      (data) => {
        this.data = data;
        this.categories = ['', ...uniq(this.data.map((d: any) => d.category))];

        this.convertGraphData(this.data);
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
        }
      },

      () => console.log('done loading posts', this.data)
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CsvUploadComponent, {
      width: '70%',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }

  uploadCsvData(csvData: any) {
    this._salesDataService.postCsvData(csvData).subscribe(
      // the first argument is a function which runs on success
      (data) => {
        console.log({data})
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
        }
      },
      // the third argument is a function which runs on completion
      () => console.log('done loading posts', this.data)
    );
  }
}
