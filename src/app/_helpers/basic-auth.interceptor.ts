import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserService } from '../user.service';
import { BACKEND_URL } from '../_helpers/constants'
@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private authenticationService: UserService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add header with basic auth credentials if user is logged in and request is to the api url
        const userToken = this.authenticationService.token;
        const isLoggedIn = userToken;
        const isApiUrl = request.url.startsWith(BACKEND_URL);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: { 
                Authorization: `JWT ${userToken}`
                }
            });
        }

        return next.handle(request);
    }
}