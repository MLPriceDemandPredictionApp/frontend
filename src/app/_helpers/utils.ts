export const getDateString = (utc: any) => {
  const date = new Date(utc);
  return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
};

interface IProps {
  headers: any[];
  predicted: any[];
  data: any[];
  predictedHeaders: any[];
}

export const formatPredictedData = (props: any) => {
  console.log({ props });
  const types = [
    ['data', 'headers'],
    ['predicted', 'predictedHeaders'],
  ];
  const mappedData: any[] = [];

  const mapDataWithHeaders = (data: any, headers: any) => {
    return data.map((row: any, index: number) => ({
      value: row ? row : null,
      name: getDateString(headers[index]),
      datetime: new Date(headers[index]),
    }));
  };

  types.map((type: any) => {
    const series: any = mapDataWithHeaders(props[type[0]], props[type[1]]);

    const trimmedSeries = series.slice(
      series.length > 6 ? series.length - 6 : 0
    );
    // .sort((a: any, b: any) => (a.datetime > b.datetime ? -1 : 1))
    // .filter((data: any) => data.value)
    // .reverse();
    mappedData.push({ name: type[0], series: trimmedSeries });
    return true;
  });

  mappedData.push({
    name: 'join',
    series: [
      {
        value: props.data[props.data.length - 1],
        name: getDateString(props.headers[props.data.length - 1]),
        datetime: new Date(props.headers[props.data.length - 1]),
      },
      {
        value: props.predicted[0],
        name: getDateString(props.predictedHeaders[0]),
        datetime: new Date(props.predictedHeaders[0]),
      },
    ],
  });

  console.log({ mappedData });
  return mappedData;
};
