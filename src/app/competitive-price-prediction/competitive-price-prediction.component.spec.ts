import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitivePricePredictionComponent } from './competitive-price-prediction.component';

describe('CompetitivePricePredictionComponent', () => {
  let component: CompetitivePricePredictionComponent;
  let fixture: ComponentFixture<CompetitivePricePredictionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompetitivePricePredictionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitivePricePredictionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
