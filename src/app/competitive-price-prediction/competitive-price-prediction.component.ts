import { CsvReadComponent } from './../components/csv-read-dialog/csv-read.component';
import { Component, OnInit } from '@angular/core';
import { SalesDataService } from '../salesdata.service';
import { UserService } from '../user.service';
import { getDateString } from '../_helpers/utils';
import { uniq, groupBy, sumBy } from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
const single = [
  {
    name: 'Germany',
    value: 8940000,
  },
  {
    name: 'USA',
    value: 5000000,
  },
  {
    name: 'France',
    value: 7200000,
  },
];
@Component({
  selector: 'app-competitive-price-prediction',
  templateUrl: './competitive-price-prediction.component.html',
  styleUrls: ['./competitive-price-prediction.component.scss'],
})
export class CompetitivePricePredictionComponent implements OnInit {
  public data: any;
  public formattedData: any;

  view: [number, number] = [800, 600];
  public csvData: any;

  public categories: any;
  public items: any;
  types = ['Price', 'Demand'];
  periods = ['yearly', 'monthly', 'weekly'];

  //settings
  public type: any = 'Price';
  public item: any;
  public category: any;
  public period: any;

  public multi: any;

  public disablePeriod: any;

  legendPosition: any = 'below';
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Timeline';
  yAxisLabel: string = this.type;
  timeline: boolean = true;

  isLinear = false;
  firstFormGroup: any;
  secondFormGroup: any;
  //bar graph props
  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  // showLegend = true;
  // showXAxisLabel = true;
  // xAxisLabel = 'Product';
  // showYAxisLabel = true;
  // yAxisLabel = 'Total';
  // single = single;
  // barPadding = 20;
  // showDataLabel = true;
  // roundEdges = true;

  // colorScheme = {
  //   domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  // };
  constructor(
    private _salesDataService: SalesDataService,
    private _userService: UserService,
    public dialog: MatDialog,
    private _formBuilder: FormBuilder
  ) {
  }


  ngOnInit(): void {
    console.log('PriceSalesVis init');
    this.getData();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  convertGraphData(data: any[]): void {
    const filteredData = this.data.filter((d: any) => d.item === this.item);
    const groupedData = groupBy(filteredData, 'item');
    const formattedData = Object.keys(groupedData).map((item: any) => ({
      name: 'Database',
      series: groupedData[item].map((date: any) => ({
        name: getDateString(date.date_time),
        value: this.type === 'Demand' ? date.demand : date.price,
      })),
    }));
    console.log({'this.csv':this.csvData, 'multi': this.multi})
    Object.assign(this, { multi: formattedData});
  }

  onSelectChange(event: any, field: any): void {
    if (field === 'category') {
      this.item = null;
    }
    this.convertGraphData(this.data);
    this.yAxisLabel = this.type;
    console.log('onselect change ', this.type, this.data, this.item, this.multi)
  }

  getData() {
    this._salesDataService.list().subscribe(
      (data) => {
        this.data = data;
        this.categories = ['', ...uniq(this.data.map((d: any) => d.category))];
        this.items = uniq(this.data.map((d: any) => d.item));
        this.convertGraphData(this.data);
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
        }
      },

      () => console.log('done loading posts', this.data)
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CsvReadComponent, {
      width: '70%',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }

  changeGraphData(data: any): void {
    Object.assign(this, { multi: data});
  }

  addCsvDataToGraph(csvData: any): void {
    console.log({ csvData });
    console.log({ multi: this.multi });
    const csvFormattedData = {
      name: 'Uploaded',
      series: csvData.map((row: any) => {
        console.log("dates:",Number(row.Year),
        Number(row.Month),
        Number(row.Date))
        const date = new Date(
          Number(row.Year),
          Number(row.Month),
          Number(row.Date)
        );
        return {
          name: getDateString(date.getUTCMilliseconds()),
          value: this.type === 'Demand' ? row.Demand : row.Price,
        };
      }),
    };

    console.log([csvFormattedData ]);
    this.csvData = csvFormattedData
    console.log({'this.csv':this.csvData, 'multi': this.multi})
    Object.assign(this, { multi: [csvFormattedData]});
  }
}
