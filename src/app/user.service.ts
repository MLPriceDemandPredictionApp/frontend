import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { BACKEND_URL } from './_helpers/constants'
 
@Injectable()
export class UserService {
 
  // http options used for making API calls
  private httpOptions: any;
 
  // the actual JWT token
  public token: any;
 
  // the token expiration date
  public token_expires: any;
 
  // the username of the logged in user
  public username: any;
 
  // error messages received from the login attempt
  public errors: any = [];

  private baseUrl: any = BACKEND_URL

  private userSubject: any;
    public user: any;
  
 
  constructor(private http: HttpClient, private router: Router) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    // this.userSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('user') || '' ));
    // this.user = this.userSubject.asObservable();
    this.token = localStorage.getItem('userToken')
    
  }

 
  // Uses http.post() to get an auth token from djangorestframework-jwt endpoint
  public login(user: any) {

    this.http.post(`${this.baseUrl}/api-token-auth/`, JSON.stringify(user), this.httpOptions).subscribe(
      (data: any) => {
        this.updateData(data['token']);
        localStorage.setItem('userToken', data['token'])
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errors = err['error'];
      }
    );
  }
 
  // Refreshes the JWT token, to extend the time the user is logged in
  public refreshToken() {
    this.http.post(`${this.baseUrl}/api-token-refresh/`, JSON.stringify({token: this.token}), this.httpOptions).subscribe(
      (data :any) => {
        console.log({data})
        this.updateData(data['token']);
        localStorage.setItem('userToken', data['token'])
        this.router.navigate(['/login']);

      },
      err => {
        this.errors = err['error'];
      }
    );
  }
 
  public logout() {
    localStorage.removeItem('userToken')
    this.token = null;
    this.token_expires = null;
    this.username = null;
    this.router.navigate(['/login']);
  }
 
  private updateData(token: any) {
    this.token = token;
    this.errors = [];
 
    // decode the token to read the username and expiration timestamp
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
  }
 
}