import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UserService } from '../user.service';

import { throwError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  /**
   * An object representing the user for the login form
   */
  public user: any;

  constructor(public _userService: UserService) {}

  ngOnInit() {
    this.user = {
      username: '',
      password: '',
    };

    console.log('login init')
  }

  login() {
    this._userService.login({
      username: this.user.username,
      password: this.user.password,
    });
  }

  refreshToken() {
    this._userService.refreshToken();
  }

  logout() {
    this._userService.logout();
  }
}
