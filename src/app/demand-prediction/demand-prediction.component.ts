import { Component, OnInit } from '@angular/core';
import { SalesDataService } from '../salesdata.service';
import { UserService } from '../user.service';
import { uniq, groupBy, sumBy } from 'lodash';
import { getDateString, formatPredictedData } from '../_helpers/utils';

const single = [
  {
    name: 'Germany',
    value: 8940000,
  },
  {
    name: 'USA',
    value: 5000000,
  },
  {
    name: 'France',
    value: 7200000,
  },
];
@Component({
  selector: 'app-demand-prediction',
  templateUrl: './demand-prediction.component.html',
  styleUrls: ['./demand-prediction.component.scss'],
})
export class DemandPredictionComponent implements OnInit {
  public data: any;
  public formattedData: any;
  public categoryMapping: any;

  view: [number, number] = [800, 600];

  public categories: any;
  public items: any;
  periods = ['yearly', 'monthly', 'weekly'];

  //settings
  public type: any = 'Demand';
  public category: any;
  public item: any;
  public period: any;

  public multi: any;

  public disablePeriod: any;

  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Time line';
  yAxisLabel: string = this.type;
  timeline: boolean = true;

  isPredicting: boolean = false;

  constructor(
    private _salesDataService: SalesDataService,
    private _userService: UserService
  ) {
    Object.assign(this, { multi: this.formattedData });
  }

  ngOnInit(): void {
    console.log('PriceSalesVis init');
    this.getData();
  }

  convertGraphData(data: any[]): void {
    const filteredData = this.data.filter((d: any) => d.item === this.item);
    const groupedData = groupBy(filteredData, 'item');
    const formattedData = Object.keys(groupedData).map((item: any) => ({
      name: item,
      series: groupedData[item].map((date: any) => ({
        name: getDateString(date.date_time),
        value: this.type === 'Demand' ? date.demand : date.price,
      })),
    }));
    Object.assign(this, { multi: formattedData });
  }

  onSelectChange(event: any, field: any): void {
    if (field === 'category') {
      this.item = null;
    }
    this.convertGraphData(this.data);
    this.yAxisLabel = this.type;
  }

  getData() {
    this._salesDataService.list().subscribe(
      (data) => {
        this.data = data;
        this.categories = uniq(this.data.map((d: any) => d.category));
        this.items = uniq(this.data.map((d: any) => d.item));
        this.convertGraphData(this.data);
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
          alert('You have been logged out. Please login again');
        }
      },

      () => console.log('done loading data', this.data)
    );
  }

  uploadCsvData(csvData: any) {
    this._salesDataService.postCsvData(csvData).subscribe(
      // the first argument is a function which runs on success
      (data) => {
        console.log({ data });
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
          alert('You have been logged out. Please login again');
        }
      },
      // the third argument is a function which runs on completion
      () => console.log('done uploading', this.data)
    );
  }

  predict() {
    console.log('predict');
    this.isPredicting = true;
    this._salesDataService.predict({ item: this.item, predictField: 'demand' }).subscribe(
      (data: any) => {
        console.log({ data });

        const mappedData = formatPredictedData({
          headers: data.headers,
          predicted: data.predictData,
          data: data.data,
          predictedHeaders: data.predictedHeaders,
        });
        console.log({ mappedData });
        Object.assign(this, { multi: mappedData });

        console.log(JSON.stringify(this.multi));
      },

      (err) => {
        console.error(err);
        if (err.status === 401) {
          this._userService.logout();
          alert('You have been logged out. Please login again');
        }
      },
      // the third argument is a function which runs on completion
      () => console.log('done predicting', this.data)
    );
    this.isPredicting = false;
  }
}
